import { Component, ViewChild } from '@angular/core';
import { BannerComponent } from 'src/app/pages/componentes-dinamicos/banner/banner.component';
import { ComponentesDinamicosDirective } from 'src/app/pages/componentes-dinamicos/componentes-dinamicos.directive';

@Component({
    selector: 'app-componentes-dinamicos',
    templateUrl: './componentes-dinamicos.component.html',
    styleUrls: ['./componentes-dinamicos.component.scss']
})
export class ComponentesDinamicosComponent {
    @ViewChild(ComponentesDinamicosDirective) diretiva!: ComponentesDinamicosDirective;

    data = [
        {
            titulo: 'Banner 1',
            mensagem: 'Mensagem do banner 1',
            cor: 'red'
        },
        {
            titulo: 'Banner 2',
            mensagem: 'Mensagem do banner 2',
            cor: 'yellow'
        },
        {
            titulo: 'Banner 3',
            mensagem: 'Mensagem do banner 3',
            cor: 'blue'
        }
    ];

    selecionarBanner(index: number): void {
        const bannerSelecionado = this.data[index];

        const viewContainerRef = this.diretiva.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent(BannerComponent);
        componentRef.instance.data = bannerSelecionado;
    }

}
