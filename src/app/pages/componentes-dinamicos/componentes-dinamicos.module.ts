import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentesDinamicosRoutingModule } from './componentes-dinamicos-routing.module';
import { ComponentesDinamicosComponent } from './componentes-dinamicos.component';
import { BannerComponent } from './banner/banner.component';
import { ComponentesDinamicosDirective } from './componentes-dinamicos.directive';


@NgModule({
  declarations: [
    ComponentesDinamicosComponent,
    BannerComponent,
    ComponentesDinamicosDirective
  ],
  imports: [
    CommonModule,
    ComponentesDinamicosRoutingModule
  ]
})
export class ComponentesDinamicosModule { }
