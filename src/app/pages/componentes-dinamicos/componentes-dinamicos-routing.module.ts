import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentesDinamicosComponent } from 'src/app/pages/componentes-dinamicos/componentes-dinamicos.component';

const routes: Routes = [
    {
        path: '',
        component: ComponentesDinamicosComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComponentesDinamicosRoutingModule { }
