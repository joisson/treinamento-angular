import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appComponentesDinamicos]'
})
export class ComponentesDinamicosDirective {

    constructor(
        public viewContainerRef: ViewContainerRef
    ) { }
}
