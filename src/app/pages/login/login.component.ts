import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    usuario!: string;
    senha!: string;
    mensagemErro = false;

    constructor(
        private router: Router
    ) { }

    ngOnInit(): void {
    }

    login(): void {
        if (this.usuario === 'teste' && this.senha === '1234') {
            window.localStorage.setItem('logado', 'true');
            this.router.navigateByUrl('/home');
        } else {
            this.mensagemErro = true;
        }
    }
}
