import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ComponentesModule } from './../../componentes/componentes.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeFilhoUmComponent } from './home-filho-um/home-filho-um.component';
import { HomeFilhoDoisComponent } from './home-filho-dois/home-filho-dois.component';


@NgModule({
    declarations: [
        HomeComponent,
        HomeFilhoUmComponent,
        HomeFilhoDoisComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        ComponentesModule
    ]
})
export class HomeModule { }
