import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExitGuard } from 'src/app/guards/exit.guard';
import { HomeFilhoDoisComponent } from 'src/app/pages/home/home-filho-dois/home-filho-dois.component';

import { HomeFilhoUmComponent } from './home-filho-um/home-filho-um.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'filho-um',
                component: HomeFilhoUmComponent
            },
            {
                path: 'filho-dois',
                component: HomeFilhoDoisComponent
            }
        ],
        canDeactivate: [
            ExitGuard
        ]
    },
    {
        path: ':id',
        component: HomeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
