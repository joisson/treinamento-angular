import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CanExit } from 'src/app/guards/exit.guard';
import {ChartVersion} from '../../enum/dashboard/chart-version.enum';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, CanExit {

    public versionForThis: ChartVersion = ChartVersion.CLASSIC;

    chart01: any = null;
    chart02: any = null;

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    ngOnInit(): void {

        this.http.get('assets/providers/chart.json').subscribe(
            res => {
                this.chart01 = JSON.parse(JSON.stringify(res));
                this.chart02 = JSON.parse(JSON.stringify(res));
            }
        );
    }

    logout(): void {
        window.localStorage.removeItem('logado');
        this.router.navigateByUrl('/login');
    }

    canExit(): any {
        return confirm('Deseja realmente sair desta tela?');
    }
}
