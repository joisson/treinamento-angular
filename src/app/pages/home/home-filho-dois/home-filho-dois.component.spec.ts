import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFilhoDoisComponent } from './home-filho-dois.component';

describe('HomeFilhoDoisComponent', () => {
  let component: HomeFilhoDoisComponent;
  let fixture: ComponentFixture<HomeFilhoDoisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeFilhoDoisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFilhoDoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
