import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFilhoUmComponent } from './home-filho-um.component';

describe('HomeFilhoUmComponent', () => {
  let component: HomeFilhoUmComponent;
  let fixture: ComponentFixture<HomeFilhoUmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeFilhoUmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFilhoUmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
