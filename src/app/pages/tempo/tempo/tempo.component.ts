import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
    selector: 'app-tempo',
    templateUrl: './tempo.component.html',
    styleUrls: ['./tempo.component.scss']
})
export class TempoComponent implements OnInit {
    dados: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private usuarioService: UsuarioService
    ) { }

    ngOnInit(): void {
        this.dados = this.activatedRoute.snapshot.data.dados;
    }
}
