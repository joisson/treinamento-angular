import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TempoComponent } from 'src/app/pages/tempo/tempo/tempo.component';

import { TempoResolver } from './../../resolver/tempo.resolver';

const routes: Routes = [
    {
        path: '',
        component: TempoComponent,
        resolve: { dados: TempoResolver }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TempoRoutingModule { }
