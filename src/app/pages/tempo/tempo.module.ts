import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TempoRoutingModule } from './tempo-routing.module';
import { TempoComponent } from './tempo/tempo.component';


@NgModule({
  declarations: [TempoComponent],
  imports: [
    CommonModule,
    TempoRoutingModule
  ]
})
export class TempoModule { }
