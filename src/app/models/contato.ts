export enum TIPOCONTATO {
    telefone = 'telefone',
    email = 'email'
}

export class Contato {
    id!: number;
    tipo!: TIPOCONTATO;
}
