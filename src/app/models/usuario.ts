import { Contato } from 'src/app/models/contato';

export class Usuario {
    id!: number;
    nome!: string;
    sobrenome!: string;
    contatos!: Contato[];
    sexo!: 'masculino' | 'feminino';
    idade?: number;

    nomeCompleto(): string {
        return this.nome + ' ' + this.sobrenome;
    }
}
