import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from 'src/app/guards/auth-guard.guard';

import { ExitGuard } from './guards/exit.guard';

const routes: Routes = [
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
    },
    {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
        canActivate: [
            AuthGuardGuard
        ],
        data: ['adminstrador', 'gestor']
    },
    {
        path: 'tempo',
        loadChildren: () => import('./pages/tempo/tempo.module').then(m => m.TempoModule),
        canActivate: [
            AuthGuardGuard
        ],
        data: ['adminstrador', 'gestor']
    },
    {
        path: 'componentes-dinamicos',
        loadChildren: () => import('./pages/componentes-dinamicos/componentes-dinamicos.module').then(m => m.ComponentesDinamicosModule),
    },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'login'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
