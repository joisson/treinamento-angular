import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appMarcaTexto]'
})
export class MarcaTextoDirective {
    @Input() appMarcaTexto = '';
    @Input() corLeave = 'transparent';

    @Output() mouseEvent = new EventEmitter<string>();

    @HostBinding('style.backgroundColor') backgroundColor!: string;

    @HostListener('mouseenter') mouseEnter(e: any): void {
        console.log(e);
        // this.renderer.setStyle(
        //     this.elementRef.nativeElement,
        //     'backgroundColor',
        //     'yellow'
        // );
        this.backgroundColor = this.appMarcaTexto;
        this.mouseEvent.emit('Mouse enter');
    }

    @HostListener('mouseleave') mouseLeave(): void {
        // this.renderer.setStyle(
        //     this.elementRef.nativeElement,
        //     'backgroundColor',
        //     'transparent'
        // );
        this.backgroundColor = this.corLeave;
        this.mouseEvent.emit('Mouse leave');
    }

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2
    ) { }
}
