import { AbstractControl } from '@angular/forms';

export class CustomValidators {
    static sexo(control: AbstractControl): { [key: string]: boolean } | null {
        const valor: string = control.value;

        if (!valor) {
            return null;
        }

        if (valor.toLowerCase() === 'f' || valor.toLowerCase() === 'm') {
            return null;
        }

        return { sexo: true };
    }
}
