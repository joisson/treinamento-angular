import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-formulario',
    templateUrl: './formulario.component.html',
    styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
    input!: FormControl;

    constructor() { }

    ngOnInit(): void {
        this.input = new FormControl('', [Validators.required, Validators.email]);
    }

    salvar(): void {
        if (this.input.invalid) {
            this.input.markAsTouched();
        } else {
            console.log('salvou');
        }
    }
}
