import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteQualquerComponent } from './componente-qualquer.component';

describe('ComponenteQualquerComponent', () => {
  let component: ComponenteQualquerComponent;
  let fixture: ComponentFixture<ComponenteQualquerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteQualquerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponenteQualquerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
