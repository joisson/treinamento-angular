import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { Component, OnInit, forwardRef, OnChanges, Input } from '@angular/core';

@Component({
    selector: 'app-custom-input',
    templateUrl: './custom-input.component.html',
    styleUrls: ['./custom-input.component.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        multi: true,
        useExisting: forwardRef(() => CustomInputComponent)
    }]
})
export class CustomInputComponent implements OnInit, ControlValueAccessor {
    @Input() label = '';
    @Input() type = 'text';
    @Input() id = '';

    control!: FormControl;

    constructor() { }

    onChanges = (obj: any) => {};
    onTouch = (obj: any) => {};

    ngOnInit(): void {
        this.control = new FormControl();

        this.control.valueChanges
            .subscribe(val => this.writeValue(val));
    }

    get value(): any {
        return this.control.value;
    }

    set value(value) {
        this.control.setValue(value, { emitEvent: false });
    }

    writeValue(obj: any): void {
        this.value = obj;
        this.onChanges(obj);
    }

    registerOnChange(fn: any): void {
        this.onChanges = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouch = fn;
    }
}
