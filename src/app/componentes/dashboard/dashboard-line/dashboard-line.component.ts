import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DashboardBaseComponent} from '../dashboard-base/dashboard-base.component';
import {ChartType} from '../../../enum/dashboard/chart-type.enum';

@Component({
  selector: 'app-dashboard-line',
  templateUrl: './../dashboard-base/dashboard-base.component.html',
  styleUrls: ['./dashboard-line.component.scss']
})
export class DashboardLineComponent extends DashboardBaseComponent implements OnChanges {

  ngOnChanges(changes: SimpleChanges): void {
    this.optionsChart = this.getDefaultOptions();
    this.tipoGrafico = ChartType.LINE;
  }
}
