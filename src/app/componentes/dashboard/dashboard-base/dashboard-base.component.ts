import {AfterViewInit, Component, Input, ViewChild} from '@angular/core';
import {ChartType} from '../../../enum/dashboard/chart-type.enum';
import {ChartVersion} from '../../../enum/dashboard/chart-version.enum';

declare const google: any;

@Component({
  selector: 'app-dashboard-base',
  templateUrl: './dashboard-base.component.html',
  styleUrls: ['./dashboard-base.component.scss']
})
export class DashboardBaseComponent implements AfterViewInit {

  @ViewChild('chart_div')
  public chartDiv: any;

  @Input()
  public altura: any = '500px';

  @Input()
  public largura: any = '1200px';

  @Input()
  public tipoGrafico: ChartType = ChartType.AREA;

  @Input()
  public version: ChartVersion = ChartVersion.CLASSIC;

  @Input()
  public listaDados: any[] = [];

  public optionsChart: any = this.getDefaultOptions();

  constructor() { }

  ngAfterViewInit(): void {

    const callbackFunction = this.drawChart;

    if (this.version === ChartVersion.CLASSIC) {
      google.charts.load('current', {packages: ['corechart', 'controls']});
    } else {
      google.charts.load('current', {packages: ['line', 'controls']});
    }

    google.charts.setOnLoadCallback(() => {
      callbackFunction(this.chartDiv, this.version, this.listaDados, this.optionsChart, this.tipoGrafico);
    });

  }

  drawChart(chartDiv: any, version: ChartVersion, dataSouce: any[], opt: any, chartType: ChartType): void {

    const dtTable = new google.visualization.DataTable();
    const colunas: any[] = [];
    if (dataSouce?.length > 0) {
      let idx = 0;
      dataSouce[0].forEach((it: any) => {
        if (idx === 0) {
          dtTable.addColumn('date', it);
        } else {
          dtTable.addColumn('number', it);
        }

        colunas.push(idx++);
      });

      dataSouce.splice(dataSouce[0], 1);
      dataSouce.forEach(itAux => {
        itAux[0] = new Date(itAux[0]);
      });
    }

    const view = new google.visualization.DataView(dtTable);
    view.setColumns(colunas);
    dtTable.addRows(dataSouce);
    //
    // const options = {
    //   title: 'Company Performance',
    //   curveType: 'function',
    //   legend: { position: 'right' }
    // };

    let chart = null;
    if (version === ChartVersion.CLASSIC) {
      if (chartType === ChartType.LINE) {
        chart = new google.visualization.LineChart(chartDiv.nativeElement);
      } else {
        chart = new google.visualization.AreaChart(chartDiv.nativeElement);
      }
    } else {
      if (chartType === ChartType.LINE) {
        chart = new google.charts.Line(chartDiv.nativeElement);
      }
    }

    chart.draw(view, opt);
  }

  getDefaultOptions(): any {

    return {
      chart: {
        title: '',
        subtitle: ''
      },
      title: '',
      subtitle: '',
      hAxis: {
        title: '',
        logScale : false
      },
      vAxis: {
        title: '',
        minValue : 0,
        logScale : true
      },
      chartArea: {
        width: '75%',
        height: '90%'
      },
      backgroundColor: 'transparent',
      curveType: 'function'
    };
  }

}
