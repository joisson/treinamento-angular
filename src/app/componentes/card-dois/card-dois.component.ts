import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, DoCheck, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-card-dois',
    templateUrl: './card-dois.component.html',
    styleUrls: ['./card-dois.component.scss']
})
export class CardDoisComponent implements OnInit, OnChanges, OnDestroy {
    @Input() titulo = '';
    @Input() corpo = '';

    @Output() changeNumero = new EventEmitter<number>();

    constructor() { }

    ngOnChanges(changes: SimpleChanges): void {
        console.log('ngOnChanges', changes);
    }

    ngOnInit(): void {
        console.log('ngOnInit');
        let count = 0;

        setInterval(() => {
            this.changeNumero.emit(++count);
        }, 1000);
    }

    ngOnDestroy(): void {
        console.log('ngOnDestroy');
    }
}
