import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDoisComponent } from './card-dois.component';

describe('CardDoisComponent', () => {
  let component: CardDoisComponent;
  let fixture: ComponentFixture<CardDoisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardDoisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
