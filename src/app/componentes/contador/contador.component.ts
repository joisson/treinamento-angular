import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-contador',
    templateUrl: './contador.component.html',
    styleUrls: ['./contador.component.scss']
})
export class ContadorComponent implements OnInit {
    count = 0;
    tabuada: number[] = [];

    constructor() { }

    ngOnInit(): void {
    }

    incrementar(): void {
        this.count++;
        this.change();
    }

    decrementar(): void {
        this.count--;
        this.change();
    }

    change(): void {
        this.tabuada = Array.from(Array(10).keys()).map(n => (n + 1));
        console.log(this.tabuada);
    }
}
