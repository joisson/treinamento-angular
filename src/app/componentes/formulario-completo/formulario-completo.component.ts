import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CustomValidators } from './../../validators/custom-validators';

@Component({
    selector: 'app-formulario-completo',
    templateUrl: './formulario-completo.component.html',
    styleUrls: ['./formulario-completo.component.scss']
})
export class FormularioCompletoComponent implements OnInit {
    form!: FormGroup;
    controls!: { [key: string]: AbstractControl };

    pessoa = {
        nome: 'Joisson',
        idade: 29,
        email: 'joisson.mello@softfocus.com.br',
        sexo: 'm',
        temTelefone: true,
        telefone: '4654653521',
        celular: '4654654635465'
    };

    constructor(
        private formBuilder: FormBuilder,
        private activatedRouter: ActivatedRoute
    ) { }

    ngOnInit(): void {
        const id = this.activatedRouter.snapshot.params.id;
        this.activatedRouter.queryParams.subscribe(val => console.log(val));

        this.form = this.formBuilder.group({
            nome: ['', [Validators.required, Validators.minLength(2)]],
            email: ['', [Validators.required, Validators.email]],
            idade: [null, [Validators.required, Validators.min(18)]],
            sexo: [null, [Validators.required, CustomValidators.sexo]],
            temTelefone: null,
            telefone: [null]
        });

        this.controls = this.form.controls;
        // this.form.setValue(this.pessoa);
        if (id) {
            this.form.patchValue(this.pessoa);
        }

        this.controls.idade.valueChanges
            .subscribe(val => {
                if (val > 20) {
                    this.controls.idade.disable({ emitEvent: false });
                }
            });

        this.controls.temTelefone.valueChanges
            .subscribe(val => {
                if (val) {
                    this.controls.telefone.setValidators(Validators.required);
                } else {
                    this.controls.telefone.clearValidators();
                }
                this.controls.telefone.updateValueAndValidity();
            });
    }

    salvar(): void {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
        } else {
            console.log(this.form.getRawValue());
        }
    }
}
