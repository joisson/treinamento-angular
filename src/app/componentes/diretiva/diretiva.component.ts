import { Component, HostListener, OnInit } from '@angular/core';

@Component({
    selector: 'app-diretiva',
    templateUrl: './diretiva.component.html',
    styleUrls: ['./diretiva.component.scss']
})
export class DiretivaComponent implements OnInit {
    aberto = false;

    @HostListener('window:click', ['$event']) windowClick(e: any): void {
        if (!e.target.closest('.botao') && !e.target.closest('.menu')) {
            this.aberto = false;
        }
    }

    constructor() { }

    ngOnInit(): void {
    }

    mensagem(texto: string): void {
        console.log(texto);
    }
}
