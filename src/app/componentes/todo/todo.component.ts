import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TodoService } from 'src/app/services/todo/todo.service';

@Component({
    selector: 'app-todo',
    templateUrl: './todo.component.html',
    styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
    @ViewChild('input') input!: ElementRef;
    // @ViewChild('contador') contador!: ContadorComponent;

    item!: string;
    pessoa: any = {};

    constructor(
        public todoService: TodoService
    ) { }

    ngOnInit(): void {
        this.todoService.lista = [];
        setTimeout(() => {
            this.pessoa.contato = {};
            this.pessoa.contato.telefone = '465465465464';
        }, 2000);

        if (this.pessoa.contato?.telefone) {
            console.log('Entrou');
        }
    }

    adicionar(): void {
        if (this.item) {
            this.todoService.adicionar(this.item);
            this.item = '';
            this.input.nativeElement.style.borderColor = 'rgb(118, 118, 118)';
            // this.contador.incrementar();
        } else {
            this.input.nativeElement.style.borderColor = 'red';
        }
    }
}
