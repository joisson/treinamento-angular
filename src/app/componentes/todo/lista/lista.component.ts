import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TodoService } from 'src/app/services/todo/todo.service';

@Component({
    selector: 'app-lista',
    templateUrl: './lista.component.html',
    styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
    @Input() item!: string;
    @Input() index!: number;

    constructor(
        private todoService: TodoService
    ) { }

    ngOnInit(): void {
    }

    excluir(): void {
        this.todoService.excluir(this.index);
    }
}
