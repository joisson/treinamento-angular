import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { noop } from 'rxjs';

import { TempoService } from './../../services/tempo/tempo.service';

@Component({
    selector: 'app-tempo',
    templateUrl: './tempo.component.html',
    styleUrls: ['./tempo.component.scss']
})
export class TempoComponent implements OnInit {
    loading = true;
    dadosTempo!: any;

    constructor(
        private tempoService: TempoService
    ) { }

    ngOnInit(): void {
        this.getLocation();
    }

    getLocation(): void {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.getPrevisao.bind(this));
        } else {
            alert('Você precisa habilitar a localização');
        }
    }

    getPrevisao(position: GeolocationPosition): void {
        const longitute = position.coords.longitude;
        const latitude = position.coords.latitude;
        const params = new HttpParams()
            .set('lat', latitude.toString())
            .set('lon', longitute.toString())
            .set('output', 'json');

        setTimeout(() => {
            this.tempoService.getPrevisao(params)
                .subscribe(response => {
                    this.dadosTempo = response;
                    this.loading = false;
                });
        }, 2000);
    }

    salvar(): void {
        const exemplo = {
            teste: 'teste',
            id: 1
        };

        this.tempoService.save(exemplo)
            .subscribe(response => console.log(response));
    }

    enviarArquivo(e: any): void {
        const file = e.target.files[0];
        const formData = new FormData();
        formData.append('teste', 'teste');
        formData.append('id', (1).toString());
        formData.append('file', file);

        this.tempoService.save(formData)
            .subscribe(response => console.log(response));
    }

    requisicaoErro(): void {
        this.tempoService.erro()
            .subscribe(
                noop,
                (err: HttpErrorResponse) => {
                    if (err.status === 403) {
                        alert('Você não tem permissão para acessar esta tela');
                    } else if ( err.status === 500) {
                        alert('Erro inesperado no servidor');
                    }
                    console.log(err.message);
                }
            );
    }
}
