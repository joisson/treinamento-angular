import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardDoisComponent } from 'src/app/componentes/card-dois/card-dois.component';
import { CardComponent } from 'src/app/componentes/card/card.component';
import { ContadorComponent } from 'src/app/componentes/contador/contador.component';
import { CustomInputComponent } from 'src/app/componentes/custom-input/custom-input.component';
import { DiretivaComponent } from 'src/app/componentes/diretiva/diretiva.component';
import { FormularioCompletoComponent } from 'src/app/componentes/formulario-completo/formulario-completo.component';
import { FormularioComponent } from 'src/app/componentes/formulario/formulario.component';
import { TempoComponent } from 'src/app/componentes/tempo/tempo.component';
import { ListaComponent } from 'src/app/componentes/todo/lista/lista.component';
import { TodoComponent } from 'src/app/componentes/todo/todo.component';
import { MarcaTextoDirective } from 'src/app/diretivas/marca-texto.directive';
import { FiltraArrayPipe } from 'src/app/pipes/filtra-array/filtra-array.pipe';
import { RelogioPipe } from 'src/app/pipes/relogio/relogio.pipe';
import { ComponenteQualquerComponent } from './componente-qualquer/componente-qualquer.component';
import { DashboardBaseComponent } from './dashboard/dashboard-base/dashboard-base.component';
import { DashboardLineComponent } from './dashboard/dashboard-line/dashboard-line.component';



@NgModule({
    declarations: [
        ContadorComponent,
        CardComponent,
        CardDoisComponent,
        TodoComponent,
        ListaComponent,
        DiretivaComponent,
        TempoComponent,
        FormularioComponent,
        FormularioCompletoComponent,
        CustomInputComponent,
        MarcaTextoDirective,
        RelogioPipe,
        FiltraArrayPipe,
        ComponenteQualquerComponent,
        DashboardBaseComponent,
        DashboardLineComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        ContadorComponent,
        CardComponent,
        CardDoisComponent,
        TodoComponent,
        ListaComponent,
        DiretivaComponent,
        TempoComponent,
        FormularioComponent,
        FormularioCompletoComponent,
        CustomInputComponent,
        DashboardBaseComponent,
        DashboardLineComponent
    ]
})
export class ComponentesModule { }
