import { DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    titulo = 'Treinamento Angular';
    corpo = 'Treinamento Angular do dia 24';
    numero!: number;
    mostraCard = true;
    hoje = new Date();
    numeroPipe = 800_000.65465;
    obj = {
        a: 1,
        b: 2
    };

    filtro = '';
    meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio',
        'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];
    loading = false;

    constructor(
        private decimalPipe: DecimalPipe,
        private router: Router,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.change(this.numeroPipe);
        setInterval(() => {
            this.hoje = new Date(this.hoje.setSeconds(this.hoje.getSeconds() + 1));
        }, 1000);

        this.router.events.subscribe(event => {
            switch (true) {
                case event instanceof NavigationStart: {
                    this.loading = true;
                    break;
                }

                case event instanceof NavigationEnd:
                case event instanceof NavigationCancel:
                case event instanceof NavigationError: {
                    this.loading = false;
                    break;
                }
                default: {
                    break;
                }
            }
        });
    }

    change(numero: number): void {
        this.numero = numero;
        console.log(this.decimalPipe.transform(this.numero, '10.7-7', 'en'));
    }

    alerta(): void {
        this.toastr.success('Alerta enviado');
    }
}
