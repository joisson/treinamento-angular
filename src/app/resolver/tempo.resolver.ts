import { TempoService } from './../services/tempo/tempo.service';
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TempoResolver implements Resolve<boolean> {
    constructor(
        private tempoService: TempoService,
        private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const params = new HttpParams()
            .set('lat', '-26.209825')
            .set('lon', '-52.6895204')
            .set('output', 'json');

        return this.tempoService.getPrevisao(params)
            .pipe(
                catchError(() => this.router.navigateByUrl('/login'))
            );
    }
}
