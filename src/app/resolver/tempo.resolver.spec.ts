import { TestBed } from '@angular/core/testing';

import { TempoResolver } from './tempo.resolver';

describe('TempoResolver', () => {
  let resolver: TempoResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(TempoResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
