import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filtraArray'
})
export class FiltraArrayPipe implements PipeTransform {

    transform(array: string[], filtro: string): string[] {
        if (!array || !array.length) {
            return array;
        }

        return array.filter(arr => arr.includes(filtro));
    }

}
