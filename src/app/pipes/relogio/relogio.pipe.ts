import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'relogio'
})
export class RelogioPipe implements PipeTransform {

    transform(data: Date): unknown {
        if (!data) {
            return data;
        }

        return data.toLocaleTimeString();
    }
}
