import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TempoService {

    constructor(
        private httpClient: HttpClient
    ) { }

    getPrevisao(params: HttpParams): Observable<any> {
        return this.httpClient.get(
            `https://www.7timer.info/bin/astro.php`, {
                params
            }
        );
    }

    save(body: any): Observable<any> {
        return this.httpClient.post('https://run.mocky.io/v3/2227dda7-c6e1-4301-b76c-a52f3099ae3d', body);
    }

    erro(): Observable<any> {
        return this.httpClient.get('https://run.mocky.io/v3/0c01ad84-42b2-42b5-8a3a-9d338fddbb41');
    }
}
