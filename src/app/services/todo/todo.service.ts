import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TodoService {
    lista: string[] = [];

    excluir(i: number): void {
        this.lista.splice(i, 1);
    }

    adicionar(item: any): void {
        this.lista.push(item);
    }

    constructor() { }
}
