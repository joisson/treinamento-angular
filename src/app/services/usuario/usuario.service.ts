import { HttpClient } from '@angular/common/http';
import { BaseService } from './../base/base.service';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';

@Injectable({
    providedIn: 'root'
})
export class UsuarioService extends BaseService<Usuario> {

    constructor(
        public http: HttpClient
    ) {
        super(
            http,
            'usuarios/'
        );
    }
}
