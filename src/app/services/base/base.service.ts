import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BaseService<M> {

    constructor(
        public http: HttpClient,
        @Inject('url') public url: string
    ) { }

    list(): Observable<M> {
        return this.http.get<M>(`${environment.apiUrl}${this.url}`);
    }

    update(body: Partial<M>, id: number): Observable<M> {
        return this.http.put<M>(`${environment.apiUrl}${this.url}${id}`, body);
    }

    create(body: M): Observable<M> {
        return this.http.post<M>(`${environment.apiUrl}${this.url}`, body);
    }

    delete(id: number): Observable<void> {
        return this.http.delete<void>(`${environment.apiUrl}${this.url}${id}`);
    }
}
