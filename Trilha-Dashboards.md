# TreinamentoAngular - Criar Dashboard com Google Chart API

Sobre a API - https://developers.google.com/chart/interactive/docs/gallery

Galeria - https://developers.google.com/chart/interactive/docs/gallery/linechart?hl=pt_br

## Objetivo
- Consumir a API de gráficos do google e criar um componente que possa ser facilmente utilizado em outras partes do sistema.

- Criar um componente que sirva para gerarmos qualquer formato de gráficos que disponibilizaremos.
  Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code

inserir api no index

\<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

ng g c componentes/dashboard/dashboard-base